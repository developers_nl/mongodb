// {{ ansible_managed }}

{% for user in mongodb_database_users %}
db = db.getSiblingDB('{{ user.database }}');
{% if user.username_file %}
user = cat({{ user.username_file | to_json }}).trim();
pwd = cat({{ user.password_file | to_json }}).trim();
{% else %}
user =  {{ user.username | to_json }}
pwd = {{ user.password | to_json }}
{% endif %}

if(db.getUser(user) === null){
    db.createUser(
        {
            user: user,
            pwd: pwd,
            roles: [{% for role in user.roles %}{{ role | to_json }}{% if not loop.last %}, {% endif %}{% endfor %}]
        }
    )
};
{% endfor %}
