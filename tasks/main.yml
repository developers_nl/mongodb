---
# tasks file for mongodb

- name: Make sure Mongodb, bash and su-exec are installed
  command: "apk add --no-cache {{ item }}"
  with_flattened:
    - mongodb
    - su-exec
    - bash
    - jq

- name: Configure mongodb
  template:
    src: mongod.conf.j2
    dest: "/etc/{{mongodb_config['config_file']}}"
    owner: root
    group: root
    mode: 0644

- name: Make sure required directories exist
  file:
    state: directory
    path: "{{ item }}"
    owner: "{{mongodb_user}}"
    group: "{{mongodb_user}}"
    mode: 0755
  with_items:
    - "{{ mongodb_conf_dbpath }}"
    - "{{ mongodb_configdb }}"
    - "{{ mongodb_conf_logpath | dirname }}"
    - "{{ mongodb_docker_entrypoint_init }}"

- name: Get installed mongo_version
  command: mongod --version
  register: mongod_version
  changed_when: false

- name: Set mongo version fact
  set_fact:
    mongo_version: "{{ mongod_version.stdout_lines[0].split(' ')[2][1:] }}"
    mongo_major_version: "{{ mongod_version.stdout_lines[0].split(' ')[2][1:].split('.')[0] }}"
    mongo_minor_version: "{{ mongod_version.stdout_lines[0].split(' ')[2][1:].split('.')[1] }}"
    mongo_patch_version: "{{ mongod_version.stdout_lines[0].split(' ')[2][1:].split('.')[2] }}"
  changed_when: false

- name: Get docker-entrypoint.sh from official mongodb docker
  get_url:
    url: "{{mongodb_docker_entrypoint_url}}"
    dest: /usr/local/bin/
    validate_certs: yes
    mode: +x

- name: Replace gosu with su-exec in docker-entrypoint.sh
  lineinfile:
    path: /usr/local/bin/docker-entrypoint.sh
    regexp: '^(\s)+exec gosu'
    line: '\1exec su-exec mongodb "$BASH_SOURCE" "$@"'
    backrefs: yes

- name: Make sure create-non-admin-users.js is present when mongodb_database_users is provided
  template:
    src: create-non-admin-users.js
    dest: "{{ mongodb_docker_entrypoint_init }}/create-non-admin-users.js"
  when: ( mongodb_database_users | length ) >= 1

- debug: msg={{ (item | splitext)[1] | regex_search('(\.(js|sh))') }}
  with_items: "{{ mongodb_init_files }}"
  when: item | splitext

- name: "Copy template files (.js or .sh ) to mongodb_docker_entrypoint_init"
  copy:
    content: "{{ lookup('template', '{{ item }}') }}"
    dest: "{{ mongodb_docker_entrypoint_init }}/{{ item }}"
  with_items: "{{ mongodb_init_files }}"
  when: (item | splitext)[1] | regex_search('(\.(js|sh))')
